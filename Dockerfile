FROM ruby:2.1
MAINTAINER Evert Verboven <e.verboven@yipyip.nl>

ARG ELIXIR_VERSION
ARG ELIXIR_DOWNLOAD_SHA256

RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get update && apt-get install -y nodejs build-essential inotify-tools esl-erlang curl ca-certificates unzip && npm install npm -g

# elixir expects utf8.
ENV ELIXIR_VERSION=$ELIXIR_VERSION\
	ELIXIR_DOWNLOAD_SHA256=$ELIXIR_DOWNLOAD_SHA256\
	LANG=C.UTF-8

RUN set -xe \
	&& ELIXIR_DOWNLOAD_URL="https://github.com/elixir-lang/elixir/releases/download/${ELIXIR_VERSION}/Precompiled.zip" \
	&& ELIXIR_DOWNLOAD_SHA256="${ELIXIR_DOWNLOAD_SHA256}"\
	&& curl -fSL -o elixir-precompiled.zip $ELIXIR_DOWNLOAD_URL \
	&& echo "$ELIXIR_DOWNLOAD_SHA256 elixir-precompiled.zip" | sha256sum -c - \
	&& unzip -d /usr/local elixir-precompiled.zip \
	&& rm elixir-precompiled.zip

RUN gem install sass -v 3.4.22
RUN gem install compass

CMD ["iex"]